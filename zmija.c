#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#include <windows.h>

//OVDE MOZES DA MENJAS VELICINU POLJA, ALI STO JE VECE POLJE
//TO JE IGRA EKSPONENCIJALNO HARDVERSKI ZAHTEVNIJA
//20 JE OTPRILIKE PRAG IGRIVOSTI, 15 JE OK

#define SIZE 15

void inicijalizujTablu(int tabla[SIZE][SIZE]);
void crtajTablu (int matrix [SIZE][SIZE]);
void victoryScreen();
void startScreen();
void failScreen();

int main(){

    int tabla[SIZE][SIZE];

    //Ovo su nizovi u kojima se cuvaju k i j koordinate tela zmije

    int ik[15];
    int jk[15];

    int i, y=0, x=1, temp=1, tempfood=0, length=3, eaten=1, ifood, jfood;

    char ch, unos;

    int zmija='*';

    DWORD start, check;

    char *food = "NEDIC";

    srand(time(NULL));

    inicijalizujTablu(tabla);

    //ovaj for popunjava pocetne koordinate za zmiju

    for (i=0; i<length; i++)
    {
        ik[i]=1;
        jk[i]=temp++;
    }

    //ovaj for popunjava tablu zmijom

    for (i=0; i<length; i++)
    {
        tabla[ik[i]][jk[i]]=zmija;
    }

    startScreen();

    crtajTablu(tabla);

    while (1)
    {
        if (eaten)
        {
            while (1)
            {
                ifood=rand()%(SIZE-2)+1;
                jfood=rand()%(SIZE-2)+1;

                for (i=0; i<length; i++)
                {
                    if (ifood==ik[i]||jfood==jk[i])
                    {
                        break;
                    }
                }
                tabla[ifood][jfood]=*(food+tempfood++);
                eaten=0;
                break;
            }
        }

        start=GetTickCount();
        check=start+250;

        while (check>GetTickCount())
            {
                if (kbhit())
                {
                    ch=getch();
                    fflush(stdin);
                    if ((ch=='A'||ch=='S'||ch=='D'||ch=='W'||ch=='a'||ch=='s'||ch=='d'||ch=='w'))
                    {
                        unos=ch;
                    }
                }
            }

        fflush(stdin);

        switch(unos)
            {
                case 'a':
                case 'A': if (x!=1) x=-1, y=0; break;
                case 'd':
                case 'D': if (x!=-1) x= 1, y=0; break;
                case 'w':
                case 'W': if (y!=1) y=-1, x=0; break;
                case 's':
                case 'S': if (y!=-1) y= 1, x=0; break;
                case 'y':
                case 'Y': system ("cls"); return 0;
            }

            //Ovaj for iscrtava zmiju

            tabla[ik[0]][jk[0]]=' ';
            for (i=1; i<length-1; i++)
            {
                tabla[ik[i]][jk[i]]=zmija;
            }
            tabla[ik[length-1]+y][jk[length-1]+x]=zmija;

            //Ovaj for apdejtuje koordinate zmije na tabli

            for (i=0; i<length-1; i++)
            {
                ik[i]=ik[i+1], jk[i]=jk[i+1];
            }
            ik[length-1]=ik[length-1]+y, jk[length-1]=jk[length-1]+x;

            if (ik[length-1]==ifood&&jk[length-1]==jfood)
            {
                eaten=1;
                length++;               //OVDE SE POVECAVA DUZINA ZMIJE
                ik[length-1]=ifood;     //I AUTOMATSKI SE UBACUJU NOVI CLANOVI U IK I JK NIZ
                jk[length-1]=jfood;
            }

            system ("cls");
            crtajTablu(tabla);

            if (tempfood==strlen(food)+1)
            {
                victoryScreen();
                return 0;
            }

            if (ik[length-1]>SIZE-2||ik[length-1]<1||jk[length-1]>SIZE-2||jk[length-1]<1)
            {
                failScreen();
                return 0;
            }
    }

    return 0;
}

//pomocne funkcije, da ne prave guzvu gore

void inicijalizujTablu(int tabla[SIZE][SIZE])
{
    int i, j;

    for (i=0; i<SIZE; i++)
    {
        for(j=0; j<SIZE; j++)
        {
            if (i==0||j==0||i==SIZE-1||j==SIZE-1)
            {
                tabla[i][j]='#';
            }
            else
            {
                tabla[i][j]=' ';
            }
        }
    }
}

void crtajTablu (int tabla [SIZE][SIZE])
{
    int i, j;

    for (i=0; i<SIZE; i++)
    {
        for (j=0; j<SIZE; j++)
        {
            printf("%c", tabla[i][j]);
        }
        putchar('\n');
    }
}

void victoryScreen()
{
    system("cls");
    putchar('\n');putchar('\n');
    printf("%28s", "YOU ATE NEDIC'S BRAIN!\n");
    printf("%20s", "YOU'RE NOW AS GOOD MATHEMATICIAN\n");
    printf("%22s", "AS HE WAS :)\n");
    putchar('\n');
    getch();getch();
}

void startScreen()
{
    system("cls");
    putchar('\n');putchar('\n');putchar('\n');
    printf("%20s", "WELCOME TO\n");
    printf("%24s", "BRAIN EATING WORM\n");
    putchar('\n');
    printf("%20s", "PRESS A/S/D/W");
    getch();
    system ("cls");
}

void failScreen()
{
    system("cls");
    putchar('\n');putchar('\n');
    printf("%32s", "YOU FAILED TO EAT NEDIC'S BRAIN!\n");
    printf("%20s", "YOU'LL NEVER BE AS GOOD MATHEMATICIAN\n");
    printf("%22s", "AS HE IS :)\n");
    putchar('\n');
    getch();getch();
}
